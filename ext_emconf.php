<?php
$EM_CONF['database'] = [
    'title' => 'Database-related methods',
    'description' => 'Typical database access methods, striving to be efficient (code-wise), '
        . 'secure and with proper error reporting',
    'category' => 'misc',
    'author' => 'Ludwig Rafelsberger',
    'author_email' => 'ludwig.rafelsberger@vitd.at',
    'author_company' => 'VITAMIN D GmbH',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.1.1',
    'constraints' => [
        'depends' => [
            'typo3' => '7.6.0-7.6.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
