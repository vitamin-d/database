<?php
namespace VITD\Database\Utility;

use TYPO3\CMS\Core\Database\PreparedStatement;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use VITD\Database\Exception\NotFoundException;

/**
 * Utility for Database access
 *
 * @author Ludwig Rafelsberger <lr@vitd.at>, VITAMIN D GmbH
 */
class Database
{

    /**
     * Cached SQL strings from files (avoid re-reads)
     *
     * Keys are (abstract) filenames
     *
     * @var array
     */
    protected static $cachedStatements = [];




    // --------------------------- public methods ---------------------------
    /**
     * Execute a SQL script from a file
     *
     * @param string $filename Input filename/filepath of SQL file to execute, may contain "EXT:" prefix
     *
     * @return void
     *
     * @throws \ErrorException 1469715219 If the SQL file cannot be read
     * @throws \ErrorException 1469715247 If SQL errors happened during execution
     */
    public static function executeSqlScript($filename)
    {
        if (! array_key_exists($filename, self::$cachedStatements)) {
            if (! is_readable(GeneralUtility::getFileAbsFileName($filename))) {
                throw new \ErrorException(sprintf('Cannot read SQL statement from file "%s" (real: "%s")',
                    $filename, GeneralUtility::getFileAbsFileName($filename)), 1469715219);
            }
            self::$cachedStatements[$filename] = file_get_contents(GeneralUtility::getFileAbsFileName($filename));
        }
        $sql = self::$cachedStatements[$filename];

        if (false === self::getDatabaseConnection()->getDatabaseHandle()->multi_query($sql)) {
            throw new \ErrorException(
                sprintf('Cannot execute SQL:' . LF . '%s' . LF . '%s', $sql,
                    self::getDatabaseConnection()->sql_error()), 1469715247);
        }
        do {
            $result = self::getDatabaseConnection()->getDatabaseHandle()->store_result();
            if ($result instanceof \mysqli_result) {
                $result->free();
            }
        } while (self::getDatabaseConnection()->getDatabaseHandle()->next_result());
    }

    /**
     * Execute a prepared statement from a SQL file
     *
     * Note that the file must contain exactly one SQL statement!
     *
     * @param string $filename Absolute or relative filename of the SQL script, may contain "EXT:" prefix
     * @param array $arguments List of arguments, either numeric array (for "?" placeholders) or associative with
     *      placeholder names as keys (e.g. ":param1" => "Value for param 1")
     * @param string $table Name of (one involved) table; This needs to be set, because TYPO3 internally rewrites the
     *      prepared statement and needs a table name to properly quote values for that table. This is a very hard
     *      misunderstanding of prepared statements.
     *
     * @return void
     *
     * @throws \ErrorException 1433752651 If the file cannot be read
     * @throws \ErrorException 1433752854 If the statement cannot be executed
     */
    public static function executePreparedStatement($filename, array $arguments = [], $table)
    {
        if (! array_key_exists($filename, self::$cachedStatements)) {
            if (! is_readable(GeneralUtility::getFileAbsFileName($filename))) {
                throw new \ErrorException(sprintf('Cannot read SQL statement from file "%s" (real: "%s")',
                    $filename, GeneralUtility::getFileAbsFileName($filename)), 1433752651);
            }
            self::$cachedStatements[$filename] = file_get_contents(GeneralUtility::getFileAbsFileName($filename));
        }

        $sql = self::$cachedStatements[$filename];

        $statement = GeneralUtility::makeInstance(PreparedStatement::class, $sql, $table);
        if (true !== $statement->execute($arguments)) {
            throw new \ErrorException(sprintf('Error executing prepared statement from file "%s": %s',
                $filename, self::getDatabaseConnection()->sql_error()), 1433752854);
        }
    }


    /**
     * Get rows from a specified database table
     *
     * @param string $table Database table name
     * @param array $columns Field names to get, all if left empty
     * @param array|string $filters Complete SQL WHERE statement as string, including all enable-fields, or a list of
     *     (string) column name => (int|string|bool) column value pairs
     *
     * @return array List of (array)rows, each being an associative array with column names as keys and column values as value
     *
     * @throws \InvalidArgumentException 1475050775 If $filter is neither a string nor an array
     * @throws \ErrorException 1475050925 If a database error occurs
     */
    public static function select($table, array $columns = [], $filters)
    {
        if (!(is_array($filters) || is_string($filters))) {
            throw new \InvalidArgumentException(sprintf('Argument "filters" for "%s" must be string or array, %s given.',
                __METHOD__, gettype($filters)), 1475050775);
        }

        $sql = sprintf(
            'SELECT %s FROM %s WHERE %s;',
            empty($columns)
                ? '*'
                : implode(', ', array_map(function ($column) { return sprintf('`%s`', $column); }, $columns)),
            $table,
            is_array($filters)
                ? implode(' AND ', array_map(function ($column) { return sprintf('`%s`=?', $column); }, array_keys($filters)))
                : $filters
        );
        $statement = GeneralUtility::makeInstance(PreparedStatement::class, $sql, $table);
        $arguments = array_values(is_array($filters) ? $filters : []);

        if (true !== $statement->execute($arguments)) {
            throw new \ErrorException('Error getting data: ' . self::getDatabaseConnection()->sql_error(), 1433752855);
        }

        return $statement->fetchAll();
    }


    /**
     * Insert a row into a specified database table
     *
     * @param string $table Database table name
     * @param array $data Field values as key=>value pairs
     *
     * @return int The uid of the just-inserted record
     *
     * @throws \ErrorException 1433752854 If a database error occurs
     */
    public static function insert($table, array $data)
    {
        $sql = sprintf(
            'INSERT INTO %s (%s) VALUES (%s);',
            $table,
            implode(', ', array_map(function ($column) { return sprintf('`%s`', $column); }, array_keys($data))),
            implode(', ', array_map(function ($column) { return sprintf(':%s', $column); }, array_keys($data))));
        $statement = GeneralUtility::makeInstance(PreparedStatement::class, $sql, $table);

        $arguments = array_combine(
            array_map(function ($column) { return sprintf(':%s', $column); }, array_keys($data)),
            array_values($data)
        );

        if (true !== $statement->execute($arguments)) {
            throw new \ErrorException('Error inserting data: ' . self::getDatabaseConnection()->sql_error(), 1433752854);
        }
        return self::getDatabaseConnection()->sql_insert_id();
    }


    /**
     * Update existing rows in a specified database table
     *
     * @param string $table Database table name
     * @param array $data Field values to be set, as key=>value pairs
     * @param array|string $filters Complete SQL WHERE statement as string, including all enable-fields, or a list of
     *     (string) column name => (int|string|bool) column value pairs
     *
     * @return void
     *
     * @throws \InvalidArgumentException 1446156272 If $filter is neither a string nor an array
     * @throws \ErrorException 1433752855 If a database error occurs
     */
    public static function update($table, array $data, $filters)
    {
        if (!(is_array($filters) || is_string($filters))) {
            throw new \InvalidArgumentException(sprintf('Argument "filters" for "%s" must be string or array, %s given.',
                __METHOD__, gettype($filters)), 1446156272);
        }

        $sql = sprintf(
            'UPDATE %s SET %s WHERE %s;',
            $table,
            implode(', ', array_map(function ($column) { return sprintf('`%s`=?', $column); }, array_keys($data))),
            is_array($filters)
                ? implode(' AND ', array_map(function ($column) { return sprintf('`%s`=?', $column); }, array_keys($filters)))
                : $filters
        );
        $statement = GeneralUtility::makeInstance(PreparedStatement::class, $sql, $table);
        $arguments = array_values(array_merge(array_values($data), is_array($filters) ? $filters : []));

        if (true !== $statement->execute($arguments)) {
            throw new \ErrorException('Error updating data: ' . self::getDatabaseConnection()->sql_error(), 1433752855);
        }
    }


    /**
     * Delete existing rows in a specified database table
     *
     * @param string $table Database table name
     * @param array|string $filters Complete SQL WHERE statement as string, including all enable-fields, or a list of
     *     (string) column name => (int|string|bool) column value pairs
     *
     * @return void
     *
     * @throws \InvalidArgumentException 1446156273 If $filter is neither a string nor an array
     * @throws \ErrorException 1433752856 If a database error occurs
     */
    public static function delete($table, $filters)
    {
        if (!(is_array($filters) || is_string($filters))) {
            throw new \InvalidArgumentException(sprintf('Argument "filters" for "%s" must be string or array, %s given.',
                __METHOD__, gettype($filters)), 1446156273);
        }

        $sql = sprintf(
            'DELETE FROM %s WHERE %s;',
            $table,
            is_array($filters)
                ? implode(' AND ', array_map(function ($column) { return sprintf('`%s`=?', $column); }, array_keys($filters)))
                : $filters
        );
        $statement = GeneralUtility::makeInstance(PreparedStatement::class, $sql, $table);
        $arguments = is_array($filters) ? $filters : [];

        if (true !== $statement->execute($arguments)) {
            throw new \ErrorException('Error deleting data: ' . self::getDatabaseConnection()->sql_error(), 1433752856);
        }
    }


    /**
     * Wrapper method to cleanly get the uid of an existing record
     *
     * @param string $table Database table name
     * @param array|string $filters Complete SQL WHERE statement as string, including all enable-fields, or a list of
     *     (string) column name => (int|string|bool) column value pairs
     *
     * @return int Uid of the specified record (first, if there is more than one)
     *
     * @throws \InvalidArgumentException 1446156271 If $filter is neither a string nor an array
     * @throws \RuntimeException 1432026207 If a database error occurs
     * @throws \VITD\Database\Exception\NotFoundException 1433234633 If the specified record cannot be found
     */
    public static function getUid($table, $filters)
    {
        if (!(is_array($filters) || is_string($filters))) {
            throw new \InvalidArgumentException(sprintf('Argument "filters" for "%s" must be string or array, %s given.',
                __METHOD__, gettype($filters)), 1446156271);
        }
        $where = is_string($filters) ? $filters : self::compileWhere($filters, $table);

        $row = self::getDatabaseConnection()->exec_SELECTgetSingleRow('uid', $table, $where);
        if (null === $row) {
            throw new \RuntimeException('DB error while getting uid of existing object: ' .
                self::getDatabaseConnection()->sql_error(), 1432026207
            );
        }
        if (is_array($row) && array_key_exists('uid', $row)) {
            return (int)$row['uid'];
        }
        throw new NotFoundException('Cannot find record to get uid from', 1433234633);
    }


    /**
     * Wrapper method to cleanly check for existence of records
     *
     * @param string $table Database table name
     * @param array|string $filters Complete SQL WHERE statement as string, including all enable-fields, or a list of
     *     (string) column name => (int|string|bool) column value pairs
     *
     * @return bool TRUE if at least one specified record exists, FALSE otherwise
     *
     * @throws \InvalidArgumentException 1446156271 If $filter is neither a string nor an array
     * @throws \RuntimeException 1432026207 If a database error occurs
     */
    public static function exists($table, $filters)
    {
        try {
            self::getUid($table, $filters);
        } catch (NotFoundException $ignored) {
            return false;
        }
        return true;
    }






    // ---------------------- internal helper methods -----------------------
    /**
     * Compile a SQL WHERE statement, conjuncting a list of column-name => value pairs
     *
     * @param array $filters List of (string) column name => (int|string|bool|float) column value pairs
     * @param string $table Database table name (used to escape string values)
     *
     * @return string Complete SQL WHERE statement equivalent to $filters
     *
     * @throws \RuntimeException 1446157067 If a value in $filters is not one of the allowed types: int|string|bool|float
     */
    protected static function compileWhere(array $filters, $table)
    {
        return
            implode(' AND ',
                array_map(
                    function ($column, $value) {
                        return sprintf('`%s`=%s', $column, $value);
                    },
                    array_keys($filters),
                    array_map(function ($value) use ($table) {
                        switch (gettype($value)) {
                            case 'integer':
                            case 'boolean':
                            case 'double':
                            case 'float': // not currently returned by PHP
                                return (string)$value;
                            case 'string':
                                return self::getDatabaseConnection()->fullQuoteStr($value, $table);
                            default:
                                // array, object, resource, NULL, unknown type
                                throw new \RuntimeException(sprintf('Type "%s" not implemented.', gettype($value)),
                                    1446157067);
                        }
                    }, $filters)
                )
            );
    }



    // ------------------------ global object access ------------------------
    /**
     * Get the TYPO3 Database handle
     *
     * @return \TYPO3\CMS\Core\Database\DatabaseConnection The TYPO3 Database handle as stored in $GLOBALS['TYPO3_DB']
     */
    protected static function getDatabaseConnection()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}
