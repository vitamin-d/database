<?php
namespace VITD\Database\Exception;

use TYPO3\CMS\Core\Exception;

/**
 * An Exception stating something has not been found whilst being expected
 *
 * @author Ludwig Rafelsberger <lr@vitd.at>, VITAMIN D GmbH
 */
class NotFoundException extends Exception
{
}
