Database
========

Typical database access methods, striving to be efficient (code-wise), secure and with proper error reporting.
The database layer you always wished TYPO3 had.


## Features

* Exceptions used where exceptions are needed, all freaky error conditions checked internally
* Secure by default - uses prepared statements for everything
* Concise. Encapsulate that silly error-handling that stems from these ancient-type of function signatures.
  If you tell the database to insert something, these methods *will fricking' insert it* or barf accordingly.
  No more nulls, minus-ones, false, or other maybes as return values.
* Offload type juggling from end user.
  Pity enough, that *I* need to tell a database what type to use. Eeeeerm, that fact alone should ring all alarm
  bells. Still, we only have what the TYPO3 and PHP gods allow us to have, thus we just swear silently and develop
  gastric ulcers. But, you're covered: These methods do that for you.
* Execute a SQL script with more than one command. One would not believe this is a problem for computers, but PHP
  sets records in making even the most basic things brainfuckingly damaged. No worries, these methods have you covered.


## Caching

Any external SQL statement from a file will be read only once per PHP request.


## Bugs

These methods have been used in different projects for now and are thoroughly real-life-tested. If there are any
remaining bugs: Please report them.
